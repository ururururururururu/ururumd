/* 
 * File:   pid.h
 * Author: funya
 *
 * Created on April 21, 2019, 12:37 AM
 */

#ifndef PID_H
#define	PID_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "mcc_generated_files/mcc.h"

#define BRAKE_DUTY 512

typedef union{
    MATHACCResult data;
    uint8_t byte[5];
    uint32_t bit32;
}PID_Result;
    
uint16_t PID_Execution(int16_t reference, int16_t actuary);

void PID_Reset();


#ifdef	__cplusplus
}
#endif

#endif	/* PID_H */

