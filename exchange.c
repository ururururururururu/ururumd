#include "exchange.h"
uint8_t SPIProcess(uint8_t byte){
    md_receive_buf.byte[DataCount] = byte;
    if(DataCount == 0){//first exchange
        inq = byte & 0x7;
        md_send.data.val = ReceiveData[inq];
   
        CRC_Initialize();
        CRC_Start();   
        for(int i = 1; i < 3; i++){
            CRC_8BitDataWrite(md_send.byte[i]);
            while(CRC_IsBusy());  
        }
        md_send.data.crc = (uint8_t)CRC_CalculatedResultGet(NORMAL, 0x00);
    }
    DataCount++;
    SSP1IF = 0;
    return md_send.byte[DataCount];
}

void EndTransaction(){
    if(RA3){
        SPI_Executed = true;
        SSP1BUF = 193;
        DataCount = 0;
    }
}
