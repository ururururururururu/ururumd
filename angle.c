#include "angle.h"
#include "exchange.h"
bool TimeOutFlag = false;

void TimerSet(){
    TMR3_Reload();
    TMR3_StartTimer();
    TimeOutFlag = false;
}

void TimeOut(){
    TimeOutFlag = true;
}

uint16_t GetAngle(){
    PEIE = 0;
    IOCIE = 0;
    
    const uint16_t  AS5601_ADDRESS = 0x36;
    uint8_t ReceiveData[2];
    uint8_t RAW_ANGLE[1];
    RAW_ANGLE[0] = 0x0C;
    uint16_t Angle = 0;
    I2C_MESSAGE_STATUS status;
    
    TRISA = 0x17;  
    SSPDATPPS = 0x11;   //RC1->MSSP:SDA;       
    RC1PPS = 0x11;      //RC1->MSSP:SDA;    
    RC2PPS = 0x10;      //RC2->MSSP:SCL;   
    SSPCLKPPS = 0x12;   //RC2->MSSP:SCL;    
    SSPSSPPS = 0x00;
    I2C_Initialize();
    I2C_Mode = true;
    PEIE = 1;  
    status = I2C_MESSAGE_PENDING;
    I2C_MasterWrite(RAW_ANGLE, 1, AS5601_ADDRESS, &status);
    TimerSet();
    while(status == I2C_MESSAGE_PENDING  && TimeOutFlag == 0);
    status = I2C_MESSAGE_PENDING;
    I2C_MasterRead(ReceiveData, 2, AS5601_ADDRESS, &status);
    TimerSet();
    while(status == I2C_MESSAGE_PENDING && TimeOutFlag == 0);
    TMR3_StopTimer();
    Angle = ((uint16_t)ReceiveData[0] << 8) & 0x0F00;
    Angle |= (uint16_t)ReceiveData[1];
    SSP1IE = 0;
    BCL1IE = 0;
    PEIE = 0;
    TRISA = 0x15;
    RC1PPS = 0x00;
    RC2PPS = 0x00;
    SSPDATPPS = 0x00;   //RA0 -> MSSP:SDI;
    SSPSSPPS = 0x03;    //RA3 -> MSSP:SS;
    SSPCLKPPS = 0x02;   //RA2 -> MSSP:SCK;
    SPI_Initialize();
    SPI_setExchangeHandler(SPIProcess);
    I2C_Mode = false;
    PEIE = 1;
    IOCIE = 1;
    //RA5 = 0;
    SSP1BUF = 193;
    DataCount = 0;
    return Angle;
}