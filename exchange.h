/* 
 * File:   exchange.h
 * Author: funya
 *
 * Created on April 21, 2019, 12:29 AM
 */

#ifndef EXCHANGE_H
#define	EXCHANGE_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "mcc_generated_files/mcc.h"

unsigned int DataCount;
bool SPI_Executed = false;

typedef enum{
  DoNothing = 0,
  Information,
  Reset,
  Brake, 
  SetDuty,
          
  PID_SetSpeed,
  PID_SetAngle,

  PID_SetGainK1,
  PID_SetGainK2,
  PID_SetGainK3,
          
  PID_AutoTuneGain,
  PID_SaveSetting,
         
}MD_Command_t;
MD_Command_t cmd;

typedef enum{
  Nothing = 0,
  RotaryEncorder,
  PulseSum,
  H_Bridge_Tempreture,
  H_Bridge_VoltageDetect,        
  PID_GainK1,
  PID_GainK2,
  PID_GainK3,
}MD_Inquire_t;
MD_Inquire_t inq;

typedef union{
  uint8_t byte[4];
  struct{
    uint8_t   dummy;
    uint16_t  val;
    uint8_t   crc;
  }data;
}MD_Send_t;

typedef union{
  uint8_t byte[4];
  struct{
    uint8_t     cmd;
    uint16_t    val;
    uint8_t     crc;
  }data;
}MD_Receive_t;

MD_Send_t    md_send_buf, md_send;
MD_Receive_t md_receive_buf, md_receive;

uint16_t ReceiveData[16];
int status;
uint8_t SPIProcess(uint8_t byte);

void EndTransaction();


#ifdef	__cplusplus
}
#endif

#endif	/* EXCHANGE_H */

