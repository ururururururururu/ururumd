/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC16F1614
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "i2c.h"
#include "exchange.h"
#include "pid.h"
#include "angle.h"
/*
                         Main application
 */

bool PID_Flag = false;
#define HEF_StartAddress 0x0f80

void PWM(uint16_t duty){
    if(duty > 962)
        duty = 962;
    else if(duty < 62)
        duty  = 62;
    PWM3_LoadDutyValue(duty);
}

void main(void)
{
    SYSTEM_Initialize();

    //Load PID_Gain-------------------------------------------------------------
    for(int i = 0; i < 3; i++){
        ReceiveData[PID_GainK1+i] = FLASH_ReadWord(HEF_StartAddress+i*2);
        ReceiveData[PID_GainK1+i] |= FLASH_ReadWord(HEF_StartAddress+1+i*2)<<8;
    }
    PID1K1H = (uint8_t) ((ReceiveData[PID_GainK1] & 0xFF00) >> 8);
    PID1K1L = (uint8_t)  (ReceiveData[PID_GainK1] & 0x00FF);
    PID1K2H = (uint8_t) ((ReceiveData[PID_GainK2] & 0xFF00) >> 8);
    PID1K2L = (uint8_t)  (ReceiveData[PID_GainK2] & 0x00FF);
    PID1K3H = (uint8_t) ((ReceiveData[PID_GainK3] & 0xFF00) >> 8);
    PID1K3L = (uint8_t)  (ReceiveData[PID_GainK3] & 0x00FF);    
    //--------------------------------------------------------------------------
    
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    
    DataCount = 0;
    TMR3_SetInterruptHandler(TimeOut);
    TMR3_StopTimer();

    SPI_setExchangeHandler(SPIProcess);
    IOCAF3_SetInterruptHandler(EndTransaction);

    SSP1BUF = 193;
    PID_Reset();
    RA5 = 0;
    
    uint16_t Buf[ERASE_FLASH_BLOCKSIZE];
    
    cmd = DoNothing;
    inq = Nothing;
    
    int LED_Time= 0;
    bool Received = false;
    while (1)
    {
        if(TMR1IF){
            TMR1IF = 0;
            TMR1_Reload();
            if(ReceiveData[H_Bridge_VoltageDetect] > 800){
                if(LED_Time > 10)
                    RA5 = !RA5;
            }
            else if(Received)
                RA5 = !RA5;
            else
                RA5 = 1;
            
            LED_Time ++;
            if(LED_Time > 10){
                RA5 = !RA5;
                LED_Time = 0;
                Received = false;
            }    
        }
        
        ReceiveData[H_Bridge_Tempreture] = ADC_GetConversion(channel_AN4);
        ReceiveData[H_Bridge_VoltageDetect] = ADC_GetConversion(channel_AN7);
        if(SPI_Executed){
            //CRC---------------------------------------------------------------
            CRC_Initialize();
            CRC_Start();
            for(int i = 0; i < DataCount; i++){
                CRC_8BitDataWrite(md_receive_buf.byte[i]);
                while(CRC_IsBusy());
            }
            if(!CRC_CalculatedResultGet(NORMAL, 0x00)){
                md_receive = md_receive_buf;    //Apply ReceiveData
                Received = true;
            }
            //------------------------------------------------------------------
            cmd = (md_receive.data.cmd >>3) & 0x1f;
            SPI_Executed = false;
            switch(cmd){
                case Brake:
                    PWM(BRAKE_DUTY);
                    break;
                case Reset:
                    PID_Reset();
                    break;
                case SetDuty:
                    PWM(md_receive.data.val);
                    break;    

                case PID_SetSpeed:  
                case PID_SetAngle:
                    if(cmd == PID_SetSpeed){
                        ReceiveData[RotaryEncorder] = (int16_t)SMT1_GetCapturedPulseWidth()-(int16_t)SMT2_GetCapturedPulseWidth();
                        ReceiveData[PulseSum] += ReceiveData[RotaryEncorder];
                        SMT1_ManualTimerReset();
                        SMT2_ManualTimerReset();
                    }
                    else if(cmd == PID_SetAngle)
                        ReceiveData[RotaryEncorder] = GetAngle();
                    PWM(PID_Execution(md_receive.data.val, ReceiveData[RotaryEncorder]));  
                    PID_Flag = false;
                    break;

                case PID_SetGainK1:
                    PID1K1H = (uint8_t) ((md_receive.data.val & 0xFF00) >> 8);
                    PID1K1L = (uint8_t)  (md_receive.data.val & 0x00FF);
                    ReceiveData[PID_GainK1] = md_receive.data.val;
                    PID_Reset();
                    break;
                case PID_SetGainK2:
                    PID1K2H = (uint8_t) ((md_receive.data.val & 0xFF00) >> 8);
                    PID1K2L = (uint8_t)  (md_receive.data.val & 0x00FF);
                    ReceiveData[PID_GainK2] = md_receive.data.val;
                    PID_Reset();
                    break;
                case PID_SetGainK3:
                    PID1K3H = (uint8_t) ((md_receive.data.val & 0xFF00) >> 8);
                    PID1K3L = (uint8_t)  (md_receive.data.val & 0x00FF);
                    ReceiveData[PID_GainK3] = md_receive.data.val;
                    PID_Reset();
                    break;
                case PID_SaveSetting:
                    for(int i = 0; i < 3; i++){
                        FLASH_WriteWord(HEF_StartAddress+i*2, Buf, ReceiveData[PID_GainK1+i]&0x00ff);
                        FLASH_WriteWord(HEF_StartAddress+1+i*2, Buf, (ReceiveData[PID_GainK1+i]>>8)&0x00ff);
                    }
                    break;
                    PID_Reset();
                default:
                    break;
            }
        }   
    }
}
/**
 End of File
*/