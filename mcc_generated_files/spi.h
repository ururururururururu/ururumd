/**
  MSSP Generated Driver API Header File

  @Company
    Microchip Technology Inc.

  @File Name
    spi.h

  @Summary
    This is the generated header file for the MSSP driver using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for MSSP.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16F1614
        Driver Version    :  1.02
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef SPI_H
#define SPI_H

/**
  Section: Included Files
*/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus  // Provide C++ Compatibility

    extern "C" {

#endif

/**
  Section: Macro Declarations
*/

#define SPI_DUMMY_DATA 0xFF

/**
  Section: SPI Module APIs
*/

/**
  @Summary
    Initializes the SPI

  @Description
    This routine initializes the SPI.
    This routine must be called before any other SPI routine is called.
    This routine should only be called once during system initialization.

  @Preconditions
    None

  @Param
    None

  @Returns
    None

  @Comment
    

  @Example
    <code>
    uint8_t     myWriteBuffer[MY_BUFFER_SIZE];
    uint8_t     myReadBuffer[MY_BUFFER_SIZE];
    uint8_t     writeData;
    uint8_t     readData;
    uint8_t     total;

    SPI_Initialize();

    total = 0;
    do
    {
        total = SPI_Exchange8bitBuffer(&myWriteBuffer[total], MY_BUFFER_SIZE - total, &myWriteBuffer[total]);

        // Do something else...

    } while(total < MY_BUFFER_SIZE);

    readData = SPI_Exchange8bit(writeData);
    </code>
 */
void SPI_Initialize(void);

/**
  @Summary
    SPI Interrupt Service Routine

  @Description
    SPI Interrupt Service Routine is called by the Interrupt Manager.

  @Returns
    None

  @Param
    None
*/
void SPI_ISR(void);

/**
  @Summary
    Set SPI Interrupt Handler

  @Description
    This sets the function to be called during the ISR

  @Preconditions
    Initialize  the SPI module with interrupt before calling this.

  @Param
    Address of function to be set

  @Returns
    None
*/
 void SPI_setExchangeHandler(uint8_t (* InterruptHandler)(uint8_t));

/**
  @Summary
    Default SPI Interrupt Handler

  @Description
    This is the default Interrupt Handler function. The data to be transmitted must be loaded into SSPBUF register by the user.

  @Preconditions
    Initialize  the SPI module with interrupt before calling this isr.

  @Param
    None

  @Returns
    None
*/
uint8_t SPI_DefaultExchangeHandler(uint8_t byte);

#ifdef __cplusplus  // Provide C++ Compatibility

    }

#endif

#endif // _SPI_H
/**
 End of File
*/
