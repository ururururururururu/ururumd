/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16F1614
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set SDI aliases
#define SDI_TRIS                 TRISAbits.TRISA0
#define SDI_LAT                  LATAbits.LATA0
#define SDI_PORT                 PORTAbits.RA0
#define SDI_WPU                  WPUAbits.WPUA0
#define SDI_OD                   ODCONAbits.ODA0
#define SDI_ANS                  ANSELAbits.ANSA0
#define SDI_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define SDI_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define SDI_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define SDI_GetValue()           PORTAbits.RA0
#define SDI_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define SDI_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define SDI_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define SDI_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define SDI_SetPushPull()        do { ODCONAbits.ODA0 = 0; } while(0)
#define SDI_SetOpenDrain()       do { ODCONAbits.ODA0 = 1; } while(0)
#define SDI_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define SDI_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set SDO aliases
#define SDO_TRIS                 TRISAbits.TRISA1
#define SDO_LAT                  LATAbits.LATA1
#define SDO_PORT                 PORTAbits.RA1
#define SDO_WPU                  WPUAbits.WPUA1
#define SDO_OD                   ODCONAbits.ODA1
#define SDO_ANS                  ANSELAbits.ANSA1
#define SDO_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define SDO_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define SDO_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define SDO_GetValue()           PORTAbits.RA1
#define SDO_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define SDO_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define SDO_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define SDO_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define SDO_SetPushPull()        do { ODCONAbits.ODA1 = 0; } while(0)
#define SDO_SetOpenDrain()       do { ODCONAbits.ODA1 = 1; } while(0)
#define SDO_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define SDO_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set SCK aliases
#define SCK_TRIS                 TRISAbits.TRISA2
#define SCK_LAT                  LATAbits.LATA2
#define SCK_PORT                 PORTAbits.RA2
#define SCK_WPU                  WPUAbits.WPUA2
#define SCK_OD                   ODCONAbits.ODA2
#define SCK_ANS                  ANSELAbits.ANSA2
#define SCK_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define SCK_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define SCK_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define SCK_GetValue()           PORTAbits.RA2
#define SCK_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define SCK_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define SCK_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define SCK_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define SCK_SetPushPull()        do { ODCONAbits.ODA2 = 0; } while(0)
#define SCK_SetOpenDrain()       do { ODCONAbits.ODA2 = 1; } while(0)
#define SCK_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define SCK_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set SS aliases
#define SS_LAT                  LATAbits.LATA3
#define SS_PORT                 PORTAbits.RA3
#define SS_WPU                  WPUAbits.WPUA3
#define SS_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define SS_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define SS_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define SS_GetValue()           PORTAbits.RA3
#define SS_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define SS_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)

// get/set IO_RA5 aliases
#define IO_RA5_TRIS                 TRISAbits.TRISA5
#define IO_RA5_LAT                  LATAbits.LATA5
#define IO_RA5_PORT                 PORTAbits.RA5
#define IO_RA5_WPU                  WPUAbits.WPUA5
#define IO_RA5_OD                   ODCONAbits.ODA5
#define IO_RA5_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define IO_RA5_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define IO_RA5_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define IO_RA5_GetValue()           PORTAbits.RA5
#define IO_RA5_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define IO_RA5_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define IO_RA5_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define IO_RA5_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define IO_RA5_SetPushPull()        do { ODCONAbits.ODA5 = 0; } while(0)
#define IO_RA5_SetOpenDrain()       do { ODCONAbits.ODA5 = 1; } while(0)

// get/set channel_AN4 aliases
#define channel_AN4_TRIS                 TRISCbits.TRISC0
#define channel_AN4_LAT                  LATCbits.LATC0
#define channel_AN4_PORT                 PORTCbits.RC0
#define channel_AN4_WPU                  WPUCbits.WPUC0
#define channel_AN4_OD                   ODCONCbits.ODC0
#define channel_AN4_ANS                  ANSELCbits.ANSC0
#define channel_AN4_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define channel_AN4_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define channel_AN4_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define channel_AN4_GetValue()           PORTCbits.RC0
#define channel_AN4_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define channel_AN4_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define channel_AN4_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define channel_AN4_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define channel_AN4_SetPushPull()        do { ODCONCbits.ODC0 = 0; } while(0)
#define channel_AN4_SetOpenDrain()       do { ODCONCbits.ODC0 = 1; } while(0)
#define channel_AN4_SetAnalogMode()      do { ANSELCbits.ANSC0 = 1; } while(0)
#define channel_AN4_SetDigitalMode()     do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set RC1 procedures
#define RC1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define RC1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define RC1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define RC1_GetValue()              PORTCbits.RC1
#define RC1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define RC1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define RC1_SetPullup()             do { WPUCbits.WPUC1 = 1; } while(0)
#define RC1_ResetPullup()           do { WPUCbits.WPUC1 = 0; } while(0)
#define RC1_SetAnalogMode()         do { ANSELCbits.ANSC1 = 1; } while(0)
#define RC1_SetDigitalMode()        do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set RC2 procedures
#define RC2_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define RC2_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define RC2_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define RC2_GetValue()              PORTCbits.RC2
#define RC2_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define RC2_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define RC2_SetPullup()             do { WPUCbits.WPUC2 = 1; } while(0)
#define RC2_ResetPullup()           do { WPUCbits.WPUC2 = 0; } while(0)
#define RC2_SetAnalogMode()         do { ANSELCbits.ANSC2 = 1; } while(0)
#define RC2_SetDigitalMode()        do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set channel_AN7 aliases
#define channel_AN7_TRIS                 TRISCbits.TRISC3
#define channel_AN7_LAT                  LATCbits.LATC3
#define channel_AN7_PORT                 PORTCbits.RC3
#define channel_AN7_WPU                  WPUCbits.WPUC3
#define channel_AN7_OD                   ODCONCbits.ODC3
#define channel_AN7_ANS                  ANSELCbits.ANSC3
#define channel_AN7_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define channel_AN7_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define channel_AN7_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define channel_AN7_GetValue()           PORTCbits.RC3
#define channel_AN7_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define channel_AN7_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define channel_AN7_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define channel_AN7_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define channel_AN7_SetPushPull()        do { ODCONCbits.ODC3 = 0; } while(0)
#define channel_AN7_SetOpenDrain()       do { ODCONCbits.ODC3 = 1; } while(0)
#define channel_AN7_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define channel_AN7_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()              PORTCbits.RC4
#define RC4_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define RC4_SetPullup()             do { WPUCbits.WPUC4 = 1; } while(0)
#define RC4_ResetPullup()           do { WPUCbits.WPUC4 = 0; } while(0)

// get/set RC5 procedures
#define RC5_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define RC5_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define RC5_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define RC5_GetValue()              PORTCbits.RC5
#define RC5_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define RC5_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define RC5_SetPullup()             do { WPUCbits.WPUC5 = 1; } while(0)
#define RC5_ResetPullup()           do { WPUCbits.WPUC5 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handler for the IOCAF3 pin functionality
 * @Example
    IOCAF3_ISR();
 */
void IOCAF3_ISR(void);

/**
  @Summary
    Interrupt Handler Setter for IOCAF3 pin interrupt-on-change functionality

  @Description
    Allows selecting an interrupt handler for IOCAF3 at application runtime
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    InterruptHandler function pointer.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF3_SetInterruptHandler(MyInterruptHandler);

*/
void IOCAF3_SetInterruptHandler(void (* InterruptHandler)(void));

/**
  @Summary
    Dynamic Interrupt Handler for IOCAF3 pin

  @Description
    This is a dynamic interrupt handler to be used together with the IOCAF3_SetInterruptHandler() method.
    This handler is called every time the IOCAF3 ISR is executed and allows any function to be registered at runtime.
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF3_SetInterruptHandler(IOCAF3_InterruptHandler);

*/
extern void (*IOCAF3_InterruptHandler)(void);

/**
  @Summary
    Default Interrupt Handler for IOCAF3 pin

  @Description
    This is a predefined interrupt handler to be used together with the IOCAF3_SetInterruptHandler() method.
    This handler is called every time the IOCAF3 ISR is executed. 
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF3_SetInterruptHandler(IOCAF3_DefaultInterruptHandler);

*/
void IOCAF3_DefaultInterruptHandler(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/