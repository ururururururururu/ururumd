#include "pid.h"
#include "exchange.h"

uint16_t PID_Execution(int16_t reference, int16_t actuary){
    PID_Result result;
    uint16_t output = 0;
    
    result.data = MATHACC_PIDControllerModeResultGet(reference, actuary);
    
    if(MATHACC_HasOverflowOccured()){
        PID_Reset();
        output = BRAKE_DUTY;
    }
    else{   
        int i,j;
        //check use data
        
        bool sign;
        
        
        if(result.data.byteU & 0x04){
            sign = false;
        }
        else{
            sign = true;
        }
        result.bit32 >>= 8;
        result.bit32 += BRAKE_DUTY;
        for(i = 31-8; i >= 10; i --){
            if((result.bit32 >> i) & 0x01){
                if(sign)
                    return 1023;
                else
                    return 0;
            }
        }
        /*
        output =(uint16_t)result.data.byteLH << 8;
        output += result.data.byteLL;
        
        output &= 0x03ff;
         */
        output = result.bit32 & 0x3ff;
    }
    return output;
}

void PID_Reset(){
    MATHACC_ClearResult();
    MATHACC_LoadZ1(0);
    MATHACC_LoadZ2(0);
}