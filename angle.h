/* 
 * File:   angle.h
 * Author: funya
 *
 * Created on 2019/04/21, 1:29
 */

#ifndef ANGLE_H
#define	ANGLE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "i2c.h"
    
bool I2C_Mode = false;
    
void TimerSet();
void TimeOut();

uint16_t GetAngle();


#ifdef	__cplusplus
}
#endif

#endif	/* ANGLE_H */

